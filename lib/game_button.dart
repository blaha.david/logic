import 'package:flutter/material.dart';

import 'game_widget.dart' show GameWidget;

typedef ButtonCallback = void Function(int rowIndex, int columnIndex);

class GameButton extends StatelessWidget {
  final ButtonCallback onBtnPressed;
  final int rowIndex;
  final int columnIndex;
  final List<int> rowValues;

  const GameButton({
    Key? key,
    required this.onBtnPressed,
    required this.rowIndex,
    required this.columnIndex,
    required this.rowValues,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Padding(
        padding: const EdgeInsets.all(4),
        child: ElevatedButton(
          onPressed: () => onBtnPressed(rowIndex, columnIndex),
          style: ElevatedButton.styleFrom(
            primary: GameWidget.colors[rowValues[columnIndex]],
            side: const BorderSide(color: Colors.black, width: 1),
          ),
          child: null,
        ),
      ),
      // ),
    );
  }
}
