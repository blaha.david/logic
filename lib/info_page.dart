import 'package:flutter/material.dart';

class InfoPage extends StatelessWidget {
  const InfoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Info'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const <Widget>[
            Text(
              'Bílá = správná barva',
              style: TextStyle(fontSize: 20),
            ),
            Text(
              'Černá = správná barva i pozice',
              style: TextStyle(fontSize: 20),
            ),
          ],
        ),
      ),
    );
  }
}
