import 'package:flutter/material.dart';

import 'game_widget.dart' show highlightColor;
import 'game_button.dart';
import 'game_feedback.dart';

class GameRow extends StatelessWidget {
  final int rowIndex;
  final ButtonCallback onBtnPressed;
  final List<int> rowValues;
  final List<Color> rating;
  final bool currentRow;

  const GameRow(
      {Key? key,
      required this.rowIndex,
      required this.onBtnPressed,
      required this.rowValues,
      required this.rating,
      required this.currentRow})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: currentRow ? highlightColor : Colors.transparent,
      child: Row(
        children: <Widget>[GameFeedback(rating: rating)] +
            List.generate(
              rowValues.length,
              (index) => GameButton(
                  onBtnPressed: onBtnPressed,
                  rowIndex: rowIndex,
                  columnIndex: index,
                  rowValues: rowValues),
            ),
      ),
    );
  }
}
