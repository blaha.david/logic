import 'package:flutter/material.dart';

import 'game_widget.dart';
import 'info_page.dart';

const String title = 'Logic';

void main() {
  runApp(const LogicApp());
}

class LogicApp extends StatefulWidget {
  const LogicApp({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LogicAppState();
}

class _LogicAppState extends State<LogicApp> {
  bool _darkTheme = false;

  void _switchTheme() {
    setState(() {
      _darkTheme = !_darkTheme;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: _darkTheme ? Brightness.dark : Brightness.light,
      ),
      home: LogicWidget(
        onSwitchTheme: _switchTheme,
      ),
    );
  }
}

class LogicWidget extends StatefulWidget {
  final Function onSwitchTheme;

  const LogicWidget({
    Key? key,
    required this.onSwitchTheme,
  }) : super(key: key);

  @override
  _LogicWidgetState createState() => _LogicWidgetState();
}

class _LogicWidgetState extends State<LogicWidget> {
  GlobalKey<GameWidgetState> gameState = GlobalKey<GameWidgetState>();

  void _restart() {
    setState(() {
      gameState.currentState!.restart();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(title),
        actions: [
          PopupMenuButton(
            onSelected: (result) {
              switch (result) {
                case 0:
                  _restart();
                  break;
                case 1:
                  widget.onSwitchTheme();
                  break;
                case 2:
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const InfoPage()),
                  );
                  break;
              }
            },
            itemBuilder: (BuildContext menuContext) => <PopupMenuEntry>[
              const PopupMenuItem(
                child: ListTile(
                  leading: Icon(Icons.add),
                  title: Text('Restart'),
                ),
                value: 0,
              ),
              const PopupMenuItem(
                child: ListTile(
                  title: Text('Switch theme'),
                ),
                value: 1,
              ),
              const PopupMenuItem(
                child: ListTile(
                  title: Text('Info'),
                ),
                value: 2,
              ),
            ],
          ),
        ],
      ),
      body: GameWidget(
        key: gameState,
        title: title,
        size: 4, // guessing 4 colors
        height: 10, // 10 attempts at guessing
      ),
    );
  }
}

class Rating {
  static const none = Colors.grey;
  static const correctColor = Colors.white;
  static const correctColorAndPosition = Colors.black;
}

