import 'package:flutter/material.dart';

import 'main.dart' show Rating;

class GameFeedback extends StatelessWidget {
  const GameFeedback({
    Key? key,
    required this.rating,
  }) : super(key: key);

  final List<Color> rating;

  @override
  Widget build(BuildContext context) {
    int len = 0;
    for (Color c in rating) {
      if (c != Rating.none) {
        len++;
      }
    }
    return AspectRatio(
      aspectRatio: 1,
      child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemCount: len,
        itemBuilder: (BuildContext ctx, index) {
          return Card(
            color: rating[index],
            shape: const RoundedRectangleBorder(
              side: BorderSide(color: Colors.black, width: 1),
            ),
          );
        },
      ),
      // ),
    );
  }
}
