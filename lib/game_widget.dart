import 'package:flutter/material.dart';
import 'dart:math' hide log;
import 'dart:developer';

import 'game_row.dart';
import 'main.dart' show Rating;

final Color highlightColor = Colors.grey.shade500.withAlpha(100);

class GameWidget extends StatefulWidget {
  static final List<Color> colors = [
    Colors.grey,
    Colors.red,
    Colors.green,
    Colors.blue,
    Colors.yellow,
    Colors.cyan,
    Colors.pink
  ];

  const GameWidget({
    Key? key,
    required this.title,
    required this.size,
    required this.height,
  }) : super(key: key);

  final int size;
  final int height;
  final String title;

  @override
  State<GameWidget> createState() => GameWidgetState();
}

class GameWidgetState extends State<GameWidget> {
  final Random rnd = Random();

  int _selectedColor = 0;
  int _currentRow = 0;

  bool _winner = false;
  bool _looser = false;

  late List<int> _goal;
  late List<List<int>> _grid;
  late List<List<Color>> _rating;

  @override
  void initState() {
    super.initState();
    restart();
  }

  void restart() {
    setState(() {
      _winner = false;
      _looser = false;

      _currentRow = widget.height - 1;

      log('Generated colors: ');
      _goal = List<int>.filled(widget.size, 0);
      for (int i = 0; i < _goal.length; i++) {
        int num = 0;
        do {
          num = rnd.nextInt(GameWidget.colors.length - 1) + 1;
        } while (_goal.contains(num));
        _goal[i] = num;
        log(num.toString());
      }

      _grid = List.generate(widget.height, (index) {
        return List.generate(widget.size, (index) => 0);
      });

      _rating = List.generate(widget.height, (index) {
        return List.generate(widget.size, (index) => Rating.none);
      });
    });
  }

  void _selectColor(int index) {
    setState(() {
      _selectedColor = index;
    });
  }

  void _onBtnPressed(int rowIndex, int columnIndex) {
    if (_winner || _looser) return;
    if (rowIndex != _currentRow) return;

    setState(() {
      for (int i = 0; i < _grid[rowIndex].length; i++) {
        if (_grid[rowIndex][i] == _selectedColor) {
          _grid[rowIndex][i] = 0;
        }
      }
      _grid[rowIndex][columnIndex] = _selectedColor;
    });
  }

  void _win() {
    setState(() {
      _winner = true;
      log('WINNER');
    });
  }

  void _loose() {
    setState(() {
      _looser = true;
      log('GAME OVER');
    });
  }

  void _confirm() {
    if (_winner || _looser) return;

    var current = _grid[_currentRow];

    for (var color in current) {
      if (color == 0) return;
    }

    setState(() {
      int correctColorAndPosition = 0;
      int correctColor = 0;

      for (int i = 0; i < widget.size; i++) {
        for (int j = 0; j < widget.size; j++) {
          if (current[i] == _goal[j]) {
            if (i == j) {
              correctColorAndPosition++;
            } else {
              correctColor++;
            }
          }
        }
      }

      log('$correctColorAndPosition correct color & position, $correctColor correct color');

      int index = 0;
      for (int i = 0; i < correctColorAndPosition; i++) {
        _rating[_currentRow][index++] = Rating.correctColorAndPosition;
      }
      for (int i = 0; i < correctColor; i++) {
        _rating[_currentRow][index++] = Rating.correctColor;
      }

      if (correctColorAndPosition == widget.size) {
        _win();
        return;
      } else if (_currentRow == 0) {
        _loose();
        return;
      }

      _currentRow--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: _winner
          ? Colors.green.shade700
          : _looser
              ? Colors.red.shade700
              : Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            Expanded(
              // Main Board
              flex: 6,
              child: AspectRatio(
                aspectRatio: (widget.size + 1) / (widget.height + 1),
                child: Column(
                  children: <Widget>[
                        // Top row that shows what colors you were guessing if you lose
                        Expanded(
                          child: Row(
                            children: List.generate(
                              widget.size + 1,
                              (index) => AspectRatio(
                                aspectRatio: 1,
                                child: Padding(
                                  padding: const EdgeInsets.all(1),
                                  child: Card(
                                    elevation: 0,
                                    color: index == 0 || !(_winner || _looser)
                                        ? Colors.transparent
                                        : GameWidget.colors[_goal[index - 1]],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ] +
                      List.generate(
                        // Rows
                        widget.height,
                        (index) => Expanded(
                          child: GameRow(
                            rowIndex: index,
                            onBtnPressed: _onBtnPressed,
                            rowValues: _grid[index],
                            rating: _rating[index],
                            currentRow: index == _currentRow,
                          ),
                        ),
                      ),
                ),
              ),
            ),
            Expanded(
              // Color selector + confirm button
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: List<Widget>.generate(
                      GameWidget.colors.length - 1,
                      (index) => buildColorButton(index),
                    ) +
                    <Widget>[
                      AspectRatio(
                        // Confirm button
                        aspectRatio: 1,
                        child: ElevatedButton(
                          onPressed: _confirm,
                          child: const FittedBox(
                            fit: BoxFit.fill,
                            child: Text(
                              "✓",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 100,
                              ),
                            ),
                          ),
                          style:
                              ElevatedButton.styleFrom(primary: Colors.white),
                        ),
                      ),
                    ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  AspectRatio buildColorButton(int index) {
    return AspectRatio(
      aspectRatio: 1,
      child: Container(
        color:
            (index + 1 == _selectedColor) ? highlightColor : Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: ElevatedButton(
            onPressed: () => _selectColor(index + 1),
            child: null,
            style:
                ElevatedButton.styleFrom(primary: GameWidget.colors[index + 1]),
          ),
        ),
      ),
    );
  }
}
